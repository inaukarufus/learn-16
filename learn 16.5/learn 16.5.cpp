﻿
#include <iostream>
#include <time.h>

int Day()
{
	struct tm newtime;
	time_t now = time(0);
	localtime_s(&newtime, &now);
	return newtime.tm_mday;
}

int main()
{
	const int N = 6;
	int array[N][N] = {};
	int date(Day() % N);
	int sum = 0;

	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			array[i][j] = i + j;
			std::cout << array[i][j];
		}
		std::cout << "\n";
	}
	
	std::cout << "\n";

	for (int j = 0; j < N; j++)
	{
		sum += array[date][j];
	}
	std::cout << sum << "\n";
}